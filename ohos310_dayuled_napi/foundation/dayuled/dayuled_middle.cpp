/*
 * Copyright (c) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include <cstring>
#include <string>
#include <memory>
#include <vector>
#include <node_api.h>
#include "x_napi_tool.h"
#include "dayuled.h"

#define NUMBER_JS_2_C(napi_v, type, dest)      \
    if (typeid(type) == typeid(int32_t))  {    \
        dest = pxt->SwapJs2CInt32(napi_v);     \
    }                                          \
    else if (typeid(type) == typeid(uint32_t)){\
        dest = pxt->SwapJs2CUint32(napi_v);    \
    }                                          \
    else if (typeid(type) == typeid(int64_t)){ \
        dest = pxt->SwapJs2CInt64(napi_v);     \
    }                                          \
    else if (typeid(type) == typeid(double_t)){\
        dest = pxt->SwapJs2CDouble(napi_v);    \
    } 
/*
static napi_value number_c_to_js(XNapiTool *pxt, const std::type_info &n, void *num)
{
    if (n == typeid(int32_t))
        return pxt->SwapC2JsInt32(*(int32_t *)num);
    else if (n == typeid(uint32_t))
        return pxt->SwapC2JsUint32(*(uint32_t *)num);
    else if (n == typeid(int64_t))
        return pxt->SwapC2JsInt64(*(int64_t *)num);
    else if (n == typeid(double_t))
        return pxt->SwapC2JsDouble(*(double_t *)num);
    return nullptr;
}
#define NUMBER_C_2_JS(pxt, n) \
    number_c_to_js(pxt, typeid(n), &n)
*/
namespace dayuled {
struct redStatusChange_value_struct {
    NUMBER_TYPE_1 in0;
    
    
};

napi_value redStatusChange_middle(napi_env env, napi_callback_info info)
{
    XNapiTool *pxt = std::make_unique<XNapiTool>(env, info).release();
    if (pxt->IsFailed()) {
        napi_value err = pxt->GetError();
        delete pxt;
        return err;
    }
    

    struct redStatusChange_value_struct *vio = new redStatusChange_value_struct();
    
    NUMBER_JS_2_C(pxt->GetArgv(0),NUMBER_TYPE_1,vio->in0);

    redStatusChange(vio->in0);

    napi_value result = nullptr;
    result = pxt->UndefinedValue();

    delete vio;
    if (pxt->IsFailed()) {
        result = pxt->GetError();
    }
    delete pxt; // release
    return result;
}
struct greenStatusChange_value_struct {
    NUMBER_TYPE_2 in0;
    
    
};

napi_value greenStatusChange_middle(napi_env env, napi_callback_info info)
{
    XNapiTool *pxt = std::make_unique<XNapiTool>(env, info).release();
    if (pxt->IsFailed()) {
        napi_value err = pxt->GetError();
        delete pxt;
        return err;
    }
    

    struct greenStatusChange_value_struct *vio = new greenStatusChange_value_struct();
    
    NUMBER_JS_2_C(pxt->GetArgv(0),NUMBER_TYPE_2,vio->in0);

    greenStatusChange(vio->in0);

    napi_value result = nullptr;
    result = pxt->UndefinedValue();

    delete vio;
    if (pxt->IsFailed()) {
        result = pxt->GetError();
    }
    delete pxt; // release
    return result;
}
struct blueStatusChange_value_struct {
    NUMBER_TYPE_3 in0;
    
    
};

napi_value blueStatusChange_middle(napi_env env, napi_callback_info info)
{
    XNapiTool *pxt = std::make_unique<XNapiTool>(env, info).release();
    if (pxt->IsFailed()) {
        napi_value err = pxt->GetError();
        delete pxt;
        return err;
    }
    

    struct blueStatusChange_value_struct *vio = new blueStatusChange_value_struct();
    
    NUMBER_JS_2_C(pxt->GetArgv(0),NUMBER_TYPE_3,vio->in0);

    blueStatusChange(vio->in0);

    napi_value result = nullptr;
    result = pxt->UndefinedValue();

    delete vio;
    if (pxt->IsFailed()) {
        result = pxt->GetError();
    }
    delete pxt; // release
    return result;
}
struct ledRGBStatusChange_value_struct {
    NUMBER_TYPE_4 in0;
    NUMBER_TYPE_5 in1;
    NUMBER_TYPE_6 in2;
    
    
};

napi_value ledRGBStatusChange_middle(napi_env env, napi_callback_info info)
{
    XNapiTool *pxt = std::make_unique<XNapiTool>(env, info).release();
    if (pxt->IsFailed()) {
        napi_value err = pxt->GetError();
        delete pxt;
        return err;
    }
    

    struct ledRGBStatusChange_value_struct *vio = new ledRGBStatusChange_value_struct();
    
    NUMBER_JS_2_C(pxt->GetArgv(0),NUMBER_TYPE_4,vio->in0);
    NUMBER_JS_2_C(pxt->GetArgv(1),NUMBER_TYPE_5,vio->in1);
    NUMBER_JS_2_C(pxt->GetArgv(2),NUMBER_TYPE_6,vio->in2);

    ledRGBStatusChange(vio->in0, vio->in1, vio->in2);

    napi_value result = nullptr;
    result = pxt->UndefinedValue();

    delete vio;
    if (pxt->IsFailed()) {
        result = pxt->GetError();
    }
    delete pxt; // release
    return result;
}}

static napi_value init(napi_env env, napi_value exports)
{
    std::shared_ptr<XNapiTool> pxt = std::make_shared<XNapiTool>(env, exports);

        pxt->DefineFunction("redStatusChange", dayuled::redStatusChange_middle);
    pxt->DefineFunction("greenStatusChange", dayuled::greenStatusChange_middle);
    pxt->DefineFunction("blueStatusChange", dayuled::blueStatusChange_middle);
    pxt->DefineFunction("ledRGBStatusChange", dayuled::ledRGBStatusChange_middle);


    return exports;
}

static napi_module g_dayuled_Module = {
    .nm_version = 1,
    .nm_flags = 0,
    .nm_filename = nullptr,
    .nm_register_func = init,
    .nm_modname = "dayuled",
    .nm_priv = ((void *)0),
    .reserved = {(void *)0},
};

extern "C" __attribute__((constructor)) void Register_dayuled_Module(void)
{
    napi_module_register(&g_dayuled_Module);
}
