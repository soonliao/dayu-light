/*
 * Copyright (c) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#ifndef IMPL_DAYULED_H
#define IMPL_DAYULED_H

#include <string>
#include <memory>
#include <vector>
#include <cmath>

using NUMBER_TYPE_1 = uint32_t;
using NUMBER_TYPE_2 = uint32_t;
using NUMBER_TYPE_3 = uint32_t;
using NUMBER_TYPE_4 = uint32_t;
using NUMBER_TYPE_5 = uint32_t;
using NUMBER_TYPE_6 = uint32_t;


namespace dayuled {
bool redStatusChange(NUMBER_TYPE_1 &status);
bool greenStatusChange(NUMBER_TYPE_2 &status);
bool blueStatusChange(NUMBER_TYPE_3 &status);
bool ledRGBStatusChange(NUMBER_TYPE_4 &r, NUMBER_TYPE_5 &g, NUMBER_TYPE_6 &b);
}

#endif // IMPL_DAYULED_H
