/*
 * Copyright (c) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include "dayuled.h"
#include "utils/log.h"

#include <fstream>
#include <memory>
#include <cstring>
#include <unistd.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <dirent.h>
namespace dayuled {

const std::string LEDS_BASE_PATH = "/sys/class/leds";
std::vector<std::string> g_ledsNodeName;
std::string g_redLedsNode = "red";
std::string g_greenLedsNode = "green";
std::string g_blueLedsNode = "blue";
int redStatus = 0;
int greenStatus = 0;
int blueStatus = 0;

void WriteLedInfoToSys(const int redbrightness, const int greenbrightness, const int bluebrightness)
{
    HILOG_INFO("[SOON] %{public}s enter", __func__);
    FILE* file = nullptr;
    std::string redLedPath = LEDS_BASE_PATH + "/" + g_redLedsNode + "/" + "brightness";
    std::string greenLedPath = LEDS_BASE_PATH + "/" + g_greenLedsNode + "/" + "brightness";
    std::string blueLedPath = LEDS_BASE_PATH + "/" + g_blueLedsNode + "/" + "brightness";
    HILOG_INFO("[SOON] %{public}s: redLedPath is %{public}s, greenLedPath is %{public}s, blueLedPath is %{public}s", __func__,
        redLedPath.c_str(), greenLedPath.c_str(), blueLedPath.c_str());

    file = fopen(redLedPath.c_str(), "w");
    if (file == nullptr) {
        HILOG_INFO("[SOON] %{public}s: red led file open failed. redLedPath is %{public}s", __func__, redLedPath.c_str());
        return;
    }
    int ret = fprintf(file, "%d\n", redbrightness);
    if (ret < 0) {
        HILOG_INFO("[SOON] %{public}s: red led file fHILOG_INFO failed.", __func__);
    }
    ret = fclose(file);
    if (ret < 0) {
        return;
    }

    file = fopen(greenLedPath.c_str(), "w");
    if (file == nullptr) {
        HILOG_INFO("[SOON] %{public}s: green led file open failed. greenLedPath is %{public}s", __func__, greenLedPath.c_str());
        return;
    }
    ret = fprintf(file, "%d\n", greenbrightness);
    if (ret < 0) {
        HILOG_INFO("[SOON] %{public}s: green led file fHILOG_INFO failed.", __func__);
    }
    ret = fclose(file);
    if (ret < 0) {
        return;
    }

    file = fopen(blueLedPath.c_str(), "w");
    if (file == nullptr) {
        HILOG_INFO("[SOON] %{public}s: blue led file open failed.", __func__);
        return;
    }
    ret = fprintf(file, "%d\n", bluebrightness);
    if (ret < 0) {
        HILOG_INFO("[SOON] %{public}s: blue led file fHILOG_INFO failed. blueLedPath is %{public}s", __func__, blueLedPath.c_str());
    }
    ret = fclose(file);
    if (ret < 0) {
        return;
    }

    HILOG_INFO("[SOON] %{public}s exit", __func__);
    return;
}

bool redStatusChange(NUMBER_TYPE_1 &status)
{
    HILOG_INFO("[SOON] redStatusChange: enter status=%{public}d redStatus=%{public}d greenStatus=%{public}d blueStatus=%{public}d",status, redStatus, greenStatus, blueStatus);
    redStatus = status;
	WriteLedInfoToSys(redStatus, greenStatus, blueStatus);
	HILOG_INFO("[SOON] redStatusChange: end ");
    return true;
}

bool greenStatusChange(NUMBER_TYPE_2 &status)
{
    HILOG_INFO("[SOON] greenStatusChange: enter status=%{public}d redStatus=%{public}d greenStatus=%{public}d blueStatus=%{public}d",status, redStatus, greenStatus, blueStatus);
    greenStatus = status;
	WriteLedInfoToSys(redStatus, greenStatus, blueStatus);
	HILOG_INFO("[SOON] greenStatusChange: end ");
    return true;
}

bool blueStatusChange(NUMBER_TYPE_3 &status)
{
    HILOG_INFO("[SOON] blueStatusChange: enter status=%{public}d redStatus=%{public}d greenStatus=%{public}d blueStatus=%{public}d",status, redStatus, greenStatus, blueStatus);
    blueStatus = status;
	WriteLedInfoToSys(redStatus, greenStatus, blueStatus);
	HILOG_INFO("[SOON] blueStatusChange: end ");
    return true;
}

bool ledRGBStatusChange(NUMBER_TYPE_4 &r, NUMBER_TYPE_5 &g, NUMBER_TYPE_6 &b)
{
    HILOG_INFO("[SOON] ledRGBStatusChange: enter redStatus=%{public}d greenStatus=%{public}d blueStatus=%{public}d", r, g, b);
	WriteLedInfoToSys(r, g, b);
	HILOG_INFO("[SOON] ledRGBStatusChange: end ");
    return true;
}
}
